from django.urls import path, include

from . import views

app_name = "login_app"

urlpatterns = [
    path(
        '', 
        views.LoginUser.as_view(),
        name='user-login',
    ),
  
]