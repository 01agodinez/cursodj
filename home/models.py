from django.db import models
from django.utils import timezone
import datetime


# Create your models here.
class Facturas(models.Model):

    ESTATUS_PAGO = [
        ("Pagada", "Pagada"),
        ("Pendiente", "Pendiente"),
    ]



    factura = models.CharField('Factura', max_length=15, unique=True, blank=True, null=True)
    razon_social = models.CharField('Razon Social', max_length=150)
    monto = models.DecimalField('Monto', max_digits=20, decimal_places=2)
    fecha_vencimiento = models.DateField('Fecha de Vencimiento', blank=True, null=True)
    pago_factura = models.CharField('Pago de Factura', max_length=10, choices= ESTATUS_PAGO, default='Pendiente')
    pdf = models.FileField('PDF', upload_to='PDF Factura', max_length=100, blank=True, null=True)
    comentarios =  models.CharField('Comentarios', max_length=200, default='-')

    @property
    def is_expired(self):
        if self.fecha_vencimiento is None:
            return True
        else:
            return self.fecha_vencimiento > datetime.date.today()
       

    class Meta:
        verbose_name = 'Factura'
        ordering = ['id']