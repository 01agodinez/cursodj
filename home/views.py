from django.shortcuts import render
from django.views.generic import ListView
from .models import Facturas



class PanelHomeView(ListView):
    template_name = "home/index.html"
    model = Facturas
    context_object_name = 'factura'
    login_url = '/'
    redirect_field_name = 'redirect_to'