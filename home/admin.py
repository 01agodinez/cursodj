from django.contrib import admin
from .models import Facturas



@admin.register(Facturas)
class FacturaAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'factura',
        'razon_social',
        'monto',
        'fecha_vencimiento',
        'pago_factura',
        'pdf',
        'comentarios',
    )


